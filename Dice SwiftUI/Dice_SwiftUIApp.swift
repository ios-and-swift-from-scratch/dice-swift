//
//  Dice_SwiftUIApp.swift
//  Dice SwiftUI
//
//  Created by Md. Ebrahim Joy on 13/1/24.
//

import SwiftUI

@main
struct Dice_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(leftDiceNumber:1, rightDiceNumber: 2)
        }
    }
}
